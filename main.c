/*
 * Copyright 2002-2010 Guillaume Cottenceau.
 *
 * This software may be freely redistributed under the terms
 * of the X11 license.
 *
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>

#include <pvm3.h>

#define PNG_DEBUG 3
#include <png.h>

void abort_(const char * s, ...)
{
        va_list args;
        va_start(args, s);
        vfprintf(stderr, s, args);
        fprintf(stderr, "\n");
        va_end(args);
        abort();
}

int x, y;

int width, height;
png_byte color_type;
png_byte bit_depth;

png_structp png_ptr;
png_infop info_ptr;
int number_of_passes;
png_bytep * row_pointers;

void read_png_file(char* file_name)
{
        char header[8];    // 8 is the maximum size that can be checked

        /* open file and test for it being a png */
        FILE *fp = fopen(file_name, "rb");
        if (!fp)
                abort_("[read_png_file] File %s could not be opened for reading", file_name);
        fread(header, 1, 8, fp);
        if (png_sig_cmp(header, 0, 8))
                abort_("[read_png_file] File %s is not recognized as a PNG file", file_name);


        /* initialize stuff */
        png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (!png_ptr)
                abort_("[read_png_file] png_create_read_struct failed");

        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr)
                abort_("[read_png_file] png_create_info_struct failed");

        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[read_png_file] Error during init_io");

        png_init_io(png_ptr, fp);
        png_set_sig_bytes(png_ptr, 8);

        png_read_info(png_ptr, info_ptr);

        width = png_get_image_width(png_ptr, info_ptr);
        height = png_get_image_height(png_ptr, info_ptr);
        color_type = png_get_color_type(png_ptr, info_ptr);
        bit_depth = png_get_bit_depth(png_ptr, info_ptr);

        number_of_passes = png_set_interlace_handling(png_ptr);
        png_read_update_info(png_ptr, info_ptr);


        /* read file */
        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[read_png_file] Error during read_image");

        row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
        for (y=0; y<height; y++)
                row_pointers[y] = (png_byte*) malloc(png_get_rowbytes(png_ptr,info_ptr));

        png_read_image(png_ptr, row_pointers);

        fclose(fp);
}


void write_png_file(char* file_name)
{
        /* create file */
        FILE *fp = fopen(file_name, "wb");
        if (!fp)
                abort_("[write_png_file] File %s could not be opened for writing", file_name);


        /* initialize stuff */
        png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (!png_ptr)
                abort_("[write_png_file] png_create_write_struct failed");

        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr)
                abort_("[write_png_file] png_create_info_struct failed");

        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[write_png_file] Error during init_io");

        png_init_io(png_ptr, fp);


        /* write header */
        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[write_png_file] Error during writing header");

        png_set_IHDR(png_ptr, info_ptr, width, height,
                     bit_depth, color_type, PNG_INTERLACE_NONE,
                     PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

        png_write_info(png_ptr, info_ptr);


        /* write bytes */
        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[write_png_file] Error during writing bytes");

        png_write_image(png_ptr, row_pointers);


        /* end write */
        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[write_png_file] Error during end of write");

        png_write_end(png_ptr, NULL);

        /* cleanup heap allocation */
        for (y=0; y<height; y++)
                free(row_pointers[y]);
        free(row_pointers);

        fclose(fp);
}

/* moj kodzik */
struct tag_name {
   int member1;
   int member2;
};


struct pixel {
    int x;
    int y;
    png_byte* data;
};

struct encodePacket {
    struct pixel* pixels;
    int messageLength;
    int pixelsAmount;
    char *message;
};

int* convertASCIIToInt(char message[], int messageLength)
{
	int *convertedMessage = malloc(sizeof (int) * messageLength);
	int i;

	for (i=0;i<messageLength;i++) {
		convertedMessage[i] = (int)message[i];
	}

	return convertedMessage;
}

char* convertIntToACSII(int* message, int messageLength)
{
	char *convertedMessage = malloc(sizeof (char) * messageLength);
	int i;

	for (i=0;i<messageLength;i++) {
		convertedMessage[i] = (char)message[i];

	}

	return convertedMessage;
}

void readMessageFromAlphaChannel()
{
	int messageIterator = 0;
	int messageLength = 1;
	int* message;
	char* charMessage;
	int i;

	for (y=0; y<height; y++) {
			png_byte* row = row_pointers[y];
			for (x=0; x<width; x++) {

				if (messageIterator < messageLength) {
					png_byte* ptr = &(row[x*4]);
					if(x == 0 && y == 0) {
						messageLength = ptr[3];
						message = malloc(sizeof (int) * messageLength);
					} else {
						message[messageIterator] = ptr[3];
						messageIterator++;
					}
				}
			}
	}

	charMessage = convertIntToACSII(message, messageLength);

	for(i=0;i<messageLength;i++){
		printf("%c\n", charMessage[i]);
	}

}

void writeMessageIntoAlphaChannel(int* message, int messageLength)
{
	int messageIterator = 0;
        for (y=0; y<height; y++) {
                png_byte* row = row_pointers[y];
                for (x=0; x<width; x++) {

					if (messageIterator < messageLength) {
						png_byte* ptr = &(row[x*4]);
						if(x == 0 && y == 0) {
							ptr[3] = messageLength;
						} else {
							ptr[3] = message[messageIterator];
							messageIterator++;
						}
					}
                }
        }
}

int* intToBitArray(int value)
{
    int i;
    int* result = malloc(sizeof (int) * 8);

    for (i = 0; i < 8; i++) {
        result[i] = value & (1 << i) ? 1 : 0;
    }

    return result;
}

int bitArrayToInt(int* array)
{
    int i;
    int result=0;

    for (i = 0; i < 8; i++) {
        result += array[i]*(int)pow(2, i);
    }

    return result;
}

void assignPixelsToPacket(struct encodePacket packet)
{

}



struct encodePacket funkcjaSymulujacaDzialanieKlientaKodujacego(struct encodePacket packet)
{
    int* messageInt = convertASCIIToInt(packet.message, packet.messageLength);
    int* characterBitArray;
    int* pixelDataBitArray;
    int i,j,k,l;

    //printf("\n\nKlient kodujacy, wiadomosc ma dlugosc %d\n", packet.messageLength);
    for(i=0,k=0;i<packet.messageLength;i++) {
        characterBitArray = intToBitArray(messageInt[i]);

        for(j=0,l=0;j<8;j++,l++) {
            if (j==4) {
                l=0;
            }
            pixelDataBitArray = intToBitArray(packet.pixels[k].data[l]);
            pixelDataBitArray[0] = characterBitArray[j];
            packet.pixels[k].data[l] = bitArrayToInt(pixelDataBitArray);

            if (j == 3 || j == 7) {
                k++;
            }

        }

    }

    return packet;
}


struct encodePacket funkcjaSymulujacaDzialanieKlientaDekodujacego(struct encodePacket packet)
{
    int* characterBitArray = malloc(sizeof (int) * 8);

    int* test = malloc(sizeof (int) * 8);

    int testd[packet.messageLength];

    //printf("\n\nKlient dekodujacy, wiadomosc ma dlugosc %d\n", packet.messageLength);
    int x,y,i,j,k,l,p;
    l=0;
    k = 0;
    for (y=0,p=0; y<height; y++) {
        png_byte* row = row_pointers[y];

        for (x=0; x<width; x++) {
            if (y == 0 && x ==0) {
                x = 4;
            }

            for (i=0;i<packet.pixelsAmount;i++) {
                if (packet.pixels[i].x == x && packet.pixels[i].y == y) {
                    png_byte* ptr = &(row[x*4]);
                    for(j=0; j<4;j++) {
                        characterBitArray = intToBitArray(ptr[j]);

                        test[l] = characterBitArray[0];

                        if (l==7) {
                            l = 0;
                            testd[p] = bitArrayToInt(test);
                            p++;
                            if (p > packet.messageLength-1) {

                                for (k=0;k<packet.messageLength;k++) {
                                    packet.message[k] = testd[k];
                                }
                                return packet;
                            }
                        } else {
                            l++;
                        }
                    }
                }
            }

        }
    }


}

void writeReceivedPixels(struct encodePacket packet)
{
    int x,y,p;

    for (y=0; y<height; y++) {
        png_byte* row = row_pointers[y];

        for (x=0; x<width; x++) {
            png_byte* ptr = &(row[x*4]);
            for (p=0; p<packet.pixelsAmount;p++) {
                if (x == packet.pixels[p].x && y == packet.pixels[p].y) {
                    ptr = packet.pixels[p].data;
                }
            }
        }
    }
}

void writeMessageLength(int messageLength)
{
    int bit1, bit2;
    if (messageLength > 255) {
        bit1 = 255;
        bit2 = 510-255;
    } else {
        bit1 = messageLength;
        bit2 = 0;
    }

    int i,k,l;
    int* bit1Array;
    int* bit2Array;
    int* pixelDataBitArray;

    int j =0;
    bit1Array = intToBitArray(bit1);
    bit2Array = intToBitArray(bit2);

    for (x=0; x<4; x++) {
        for (y=0; y<1; y++) {
            png_byte* row = row_pointers[y];
            png_byte* ptr = &(row[x*4]);

            for (i=0; i<4;i++) {
                pixelDataBitArray = intToBitArray(ptr[i]);

                if (x < 2) {
                    pixelDataBitArray[0] = bit1Array[j];
                    j++;
                } else {
                    pixelDataBitArray[0] = bit2Array[j];
                    j++;
                }

                ptr[i] = bitArrayToInt(pixelDataBitArray);

                if (j>7) {
                    j=0;
                }
            }

        }
    }
}

int readMessageLength()
{
    int bit1=0, bit2=0;
    int i,k,l;
    int* bit1Array;
    int* bit2Array;
    int* pixelDataBitArray;
    int messageLength;
    int j =0;
    bit1Array = intToBitArray(bit1);
    bit2Array = intToBitArray(bit2);

    for (x=0; x<4; x++) {
        for (y=0; y<1; y++) {
            png_byte* row = row_pointers[y];
            png_byte* ptr = &(row[x*4]);

            for (i=0; i<4;i++) {
                pixelDataBitArray = intToBitArray(ptr[i]);

                if (x < 2) {
                    bit1Array[j] = pixelDataBitArray[0];
                    j++;
                } else {
                    bit2Array[j] = pixelDataBitArray[0];
                    j++;
                }



                if (j>7) {
                    j=0;
                }
            }

        }
    }

    messageLength = bitArrayToInt(bit1Array) + bitArrayToInt(bit2Array);

    return messageLength;
}

void readReceivedPixels(int messageLength)
{
    int* characterBitArray = malloc(sizeof (int) * 8);

    int* test = malloc(sizeof (int) * 8);

    int testd[messageLength];

    int x,y,i,j,p;
    j = 0;
    for (y=0,p=0; y<height; y++) {
        png_byte* row = row_pointers[y];

        for (x=0; x<width; x++) {
            if (y == 0 && x ==0) {
                x = 4;
            }
            png_byte* ptr = &(row[x*4]);

            for(i=0; i<4;i++) {

                characterBitArray = intToBitArray(ptr[i]);
                test[j] = characterBitArray[0];
                if(j==7) {
                    j = 0;
                    testd[p] = bitArrayToInt(test);
                    printf("%d\n", testd[p]);
                    p++;
                    if (p > messageLength-1) {
                        return;
                    }
                } else {
                    j++;
                }
            }

            //printf("\n\n\n\n");
        }
    }
}


int sendPackage(char **bitArraySend, int arraySize, int compsNumber, int type)
{
    int tids[18];
    int nproc;
    	nproc = pvm_spawn("slaveIMG", NULL, PvmTaskDefault, "", compsNumber , tids);
    int i;
    int info[10];
    static char *hosts[] = {
                                "e5-s19-1",
                                "e5-s19-2",
                                "e5-s19-3",
                                "e5-s19-4",
                                "e5-s19-5",
                                "e5-s19-6",
                                "e5-s19-7",
                                "e5-s19-8",
                                "e5-s19-9",
                                "e5-s19-10"//,
                                // "e5-s19-11",
                                // "e5-s19-12",
                                // "e5-s19-13",
                                // "e5-s19-14"
                            };
    pvm_addhosts(hosts, 10, info);

    for(i=0;i<compsNumber;i++)
    {
        pvm_initsend(PvmDataDefault);
        int buff = pvm_mkbuf(1);
        int pack = pvm_pkint( bitArraySend, arraySize, 1);
        int send = pvm_send( tids[i], 1);

    }

    for(i=0;i<compsNumber;i++)
    {
        pvm_initsend(PvmDataDefault);
		int bufid = pvm_recv(tids[i], 1);
        pvm_upkstr(*bitArraySend[i]);
    }

    pvm_exit();
}




int main(int argc, char **argv)
{
    png_byte* row;
    png_byte* ptr;

    if(strcmp(argv[3], "encode") == 0) {

        read_png_file(argv[1]);

        /**
         * ustalam zmienne
         */
        int i,j,k,p;
        int clients = 0;

        for (i = 0; argv[2][i] != '\0'; i++) {
            clients *= 10;
            clients += argv[2][i] - '0';
        }
        int messageLength = strlen(argv[4]);
        int messageLengthPerClient = messageLength/clients;
        int messageLengthMod = messageLength%clients;
        struct encodePacket packetsToSend[clients];
        struct encodePacket receivedPackets[clients];

        if (messageLength < clients) {
            printf("\ndlugosc wiadomosci musi byc >= od ilosci klientow\n\n");
            return 0;
        } else if (messageLength > 510) {
            printf("\nwiadomosc nie moze byc dluzsza niz 510 znakow\n\n");
            return 0;
        }

        /**
         * dziele podany string na tyle czesci ile jest klientow, ostatni klient dostanie
         * wiekszy string, jesli dlugosc stringa nie jest podzielna przez ilosc klientow
         */
        for(i=0,k=0;i<clients;i++) {
            packetsToSend[i].messageLength = messageLengthPerClient;
            if (i+1 == clients) {
                packetsToSend[i].messageLength += messageLengthMod;
            }

            packetsToSend[i].pixelsAmount = packetsToSend[i].messageLength*2;


            packetsToSend[i].message = malloc(sizeof (char) * packetsToSend[i].messageLength);

            for(j=0;j<packetsToSend[i].messageLength;j++,k++) {
                packetsToSend[i].message[j] = argv[4][k];
            }


            packetsToSend[i].pixels = malloc(sizeof (struct pixel) * packetsToSend[i].pixelsAmount);
        }

        for (y=0,i=0,p=0; y<height; y++) {
            if (p > packetsToSend[i].pixelsAmount-1) {
                i++;
                p=0;

                if (i > clients-1) {
                    break;
                }
            }
            row = row_pointers[y];
            for (x=0; x<width; x++,p++) {
                if (y == 0 && x ==0) {
                    x = 4;
                }

                if (p > packetsToSend[i].pixelsAmount-1) {
                    i++;
                    p=0;

                    if (i > clients-1) {
                        break;
                    }
                }
                if (i > clients-1) {
                    break;
                }
                ptr = &(row[x*4]);

                packetsToSend[i].pixels[p].x = x;
                packetsToSend[i].pixels[p].y = y;
                packetsToSend[i].pixels[p].data = ptr;

            }
        }

        for(i=0;i<clients;i++) {
            receivedPackets[i] = funkcjaSymulujacaDzialanieKlientaKodujacego(packetsToSend[i]);

            writeReceivedPixels(receivedPackets[i]);
        }

        writeMessageLength(messageLength);
        write_png_file(argv[1]);
    }else if(strcmp(argv[3], "decode") == 0) {

        read_png_file(argv[1]);

        int i,j,k,p;
        int clients = 0;

        for (i = 0; argv[2][i] != '\0'; i++) {
            clients *= 10;
            clients += argv[2][i] - '0';
        }
        int messageLength = readMessageLength();
        int messageLengthPerClient = messageLength/clients;
        int messageLengthMod = messageLength%clients;
        struct encodePacket packetsToSend[clients];
        struct encodePacket receivedPackets[clients];

        for(i=0,k=0;i<clients;i++) {
            packetsToSend[i].messageLength = messageLengthPerClient;
            if (i+1 == clients) {
                packetsToSend[i].messageLength += messageLengthMod;
            }

            packetsToSend[i].pixelsAmount = packetsToSend[i].messageLength*2;


            packetsToSend[i].message = malloc(sizeof (char) * packetsToSend[i].messageLength);

            packetsToSend[i].pixels = malloc(sizeof (struct pixel) * packetsToSend[i].pixelsAmount);
        }

        for (y=0,i=0,p=0; y<height; y++) {
            if (p > packetsToSend[i].pixelsAmount-1) {
                i++;
                p=0;

                if (i > clients-1) {
                    break;
                }
            }
            row = row_pointers[y];
            for (x=0; x<width; x++,p++) {
                if (y == 0 && x ==0) {
                    x = 4;
                }

                    if (p > packetsToSend[i].pixelsAmount-1) {
                        i++;
                        p=0;

                        if (i > clients-1) {
                            break;
                        }
                    }
                    if (i > clients-1) {
                        break;
                    }
                    ptr = &(row[x*4]);

                    packetsToSend[i].pixels[p].x = x;
                    packetsToSend[i].pixels[p].y = y;
                    packetsToSend[i].pixels[p].data = ptr;

            }
        }

        for(i=0;i<clients;i++) {
            receivedPackets[i] = funkcjaSymulujacaDzialanieKlientaDekodujacego(packetsToSend[i]);
        }

        printf("\n*****\n\n");
        for(i=0;i<clients;i++) {
            for(j=0;j<receivedPackets[i].messageLength;j++) {
                printf("%c", receivedPackets[i].message[j]);
            }
        }
        printf("\n\n*****\n\n");
    }

    return 0;
}
